package com.elchin.userform.exception;

import com.elchin.userform.error.ErrorResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = ImageNotFoundException.class)
    public ResponseEntity<ErrorResponse> imageNotFoundExceptionHandler() {
        return null;
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        System.out.println("handleMethodArgumentNotValid worked");
        List<String> list = new ArrayList<>();
        ex.getBindingResult().getAllErrors().forEach(objectError -> {
            list.add(objectError.getDefaultMessage());
        });

        ErrorResponse errorResponse = new ErrorResponse("Method Argument Not Valid", list);

        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = UserNotFoundException.class)
    public ResponseEntity<ErrorResponse> userNotFoundExceptionHandler(UserNotFoundException us) {
        ErrorResponse errorResponse = new ErrorResponse(us.getMessage(), new ArrayList<>());
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }
}
