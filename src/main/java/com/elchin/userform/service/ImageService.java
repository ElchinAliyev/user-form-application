package com.elchin.userform.service;

import com.elchin.userform.domain.User;
import com.elchin.userform.exception.ImageNotFoundException;
import com.elchin.userform.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class ImageService {
    private final String path = "C:\\Users\\nadir\\Desktop\\user-form-images" + File.separator;
    private int count = 0;

    @Autowired
    private UserRepository userRepository;

    public String saveImage(MultipartFile multipartFile) {
        Path imagePath = null;
        try {
            System.out.println(multipartFile.getOriginalFilename());
            byte[] bytes = multipartFile.getBytes();
            count++;
            imagePath = Paths.get(path + count + getFileExtension(multipartFile.getOriginalFilename()));
            Files.write(imagePath, bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imagePath.toString();
    }

    public Resource getImage(int id) {
        User user = userRepository.getOne(id);
        String address = user.getImageAddress();
        Resource resource = null;
        System.out.println(address);
        try {
            resource = new UrlResource("file:///" +address);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return resource;
    }

    private String getFileExtension(String fileName) {
        int x = fileName.lastIndexOf(".");
        return fileName.substring(x);
    }


}
