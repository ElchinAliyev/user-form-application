package com.elchin.userform.service;

import com.elchin.userform.domain.User;
import com.elchin.userform.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;


    public User insertUser(User user) {
        return userRepository.save(user);
    }


    public Optional<User> getUserById(int id) {
        return userRepository.findById(id);
    }

    public User addUserImage(int id, String address) {
        User user = userRepository.getOne(id);
        user.setImageAddress(address);
        userRepository.save(user);
        return user;
    }
}
