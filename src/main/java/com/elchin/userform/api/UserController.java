package com.elchin.userform.api;

import com.elchin.userform.domain.User;
import com.elchin.userform.exception.UserNotFoundException;
import com.elchin.userform.service.ImageService;
import com.elchin.userform.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Optional;

@RestController
@Validated
@CrossOrigin
public class UserController {

    @Autowired
    private ImageService imageService;
    @Autowired
    private UserService userService;
    @Autowired
    private Environment environment;


    @RequestMapping(value = "/create/user", method = RequestMethod.POST)
    public ResponseEntity<User> insertUser(@Valid @RequestBody User user) {
        userService.insertUser(user);
        return new ResponseEntity<>(user, HttpStatus.CREATED);
    }


    @RequestMapping(value = "/user/{id}/image", method = RequestMethod.PUT)
    public ResponseEntity<User> uploadImage(@NotNull @PathVariable int id,
                                            @RequestParam(value = "image") @NotNull(message = "Image can not be null") MultipartFile multipartFile) {

        Optional<User> optionalUser = userService.getUserById(id);
        if (optionalUser.isPresent()) {
            String address = imageService.saveImage(multipartFile);
            User user = userService.addUserImage(id, address);
            return new ResponseEntity<>(user, HttpStatus.OK);
        }

        throw new UserNotFoundException("User not found with this id : " + id);
    }


    @GetMapping("/user/{id}")
    public ResponseEntity<User> getUser(@PathVariable int id) {
        Optional<User> optionalUser = userService.getUserById(id);

        if (optionalUser.isPresent()) {
            return ResponseEntity.ok(optionalUser.get());
        }
        throw new UserNotFoundException("User not found with this id : " + id);
    }


    @GetMapping("user/{id}/image")
    public ResponseEntity<Resource> getImage(@PathVariable int id, HttpServletRequest request) {

        Optional<User> optionalUser = userService.getUserById(id);

        if (optionalUser.isPresent()) {
            Resource resource = imageService.getImage(id);
            String contentType = null;
            try {
                contentType = Files.probeContentType(resource.getFile().toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.add("content-type", contentType);

            return new ResponseEntity<Resource>(resource, httpHeaders, HttpStatus.OK);

        }
        throw new UserNotFoundException("User Not Found with this id : " + id);

    }


}
