package com.elchin.userform.error;

import java.util.List;

public class ErrorResponse {
    private String errorName;
    private List<String> errorList;

    public String getErrorName() {
        return errorName;
    }

    public ErrorResponse() {
    }

    public ErrorResponse(String errorName, List<String> errorList) {
        this.errorName = errorName;
        this.errorList = errorList;
    }

    public void setErrorName(String errorName) {
        this.errorName = errorName;
    }

    public List<String> getErrorList() {
        return errorList;
    }

    public void setErrorList(List<String> errorList) {
        this.errorList = errorList;
    }
}
