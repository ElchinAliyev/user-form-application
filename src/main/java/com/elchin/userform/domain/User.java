package com.elchin.userform.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "user_name")
public class User {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @NotBlank
    @Size(min = 2, max = 10, message = "Your name must be min size 2 , max size 10")
    @Column(nullable = false)
    private String name;
    @NotBlank
    @Size(min = 2, max = 10, message = "Your surname must be min size 2 , max size 10")
    @Column(nullable = false)
    private String surname;

    @Min(value = 18, message = "Your age must be between 18 and 65")
    @Max(value = 65, message = "Your age must be between 18 and 65")
    private int age;
    private String imageAddress;


    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", imageAddress='" + imageAddress + '\'' +
                '}';
    }

    public User() {
    }

    public User(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getImageAddress() {
        return imageAddress;
    }

    public void setImageAddress(String imageAddress) {
        this.imageAddress = imageAddress;
    }
}
