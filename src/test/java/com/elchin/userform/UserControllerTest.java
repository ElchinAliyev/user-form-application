package com.elchin.userform;

import com.elchin.userform.domain.User;
import com.elchin.userform.service.ImageService;
import com.elchin.userform.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.collection.IsCollectionWithSize;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest
public class UserControllerTest {

    @MockBean
    private UserService userService;
    @MockBean
    private ImageService imageService;
    @Autowired
    private MockMvc mockMvc;
    private User user = null;
    private ObjectMapper objectMapper = null;


    @Before
    public void setup() {
        objectMapper = new ObjectMapper();
        objectMapper = new ObjectMapper();
        user = new User();
        user.setName("Hezi");
        user.setSurname("Abbasov");
        user.setAge(20);

    }

    @Test
    public void testInsertUser() throws Exception {
        String jsonData = objectMapper.writeValueAsString(user);
        Mockito.when(userService.insertUser(ArgumentMatchers.any(User.class))).thenReturn(user);
        mockMvc.perform(MockMvcRequestBuilders.post("/create/user")
                .contentType("application/json")
                .content(jsonData))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.content().contentType("application/json"))
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Is.is("Hezi")));

        Mockito.verify(userService, VerificationModeFactory.times(1)).insertUser(ArgumentMatchers.any(User.class));
    }

    @Test
    public void testInsertUserFail() throws Exception {
        User user1 = new User();
        user1.setName("H");
        user1.setSurname("Abbasov");
        user1.setAge(20);
        String jsonData = objectMapper.writeValueAsString(user1);
        mockMvc.perform(MockMvcRequestBuilders.post("/create/user").contentType(MediaType.APPLICATION_JSON)
                .content(jsonData))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorList", IsCollectionWithSize.hasSize(1)));

        Mockito.verifyNoInteractions(userService);
    }


    @Test
    public void testGetUser() throws Exception {
        Mockito.when(userService.getUserById(ArgumentMatchers.any(Integer.class))).thenReturn(Optional.of(user));

        mockMvc.perform(MockMvcRequestBuilders.get("/user/5"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.surname", Is.is("Abbasov")));

        Mockito.verify(userService, VerificationModeFactory.times(1)).getUserById(ArgumentMatchers.any(Integer.class));
    }


    @Test
    public void testGetUserFail() throws Exception {

        Mockito.when(userService.getUserById(ArgumentMatchers.any(Integer.class))).thenReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders.get("/user/5"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorName", Is.is("User not found with this id : 5")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorList").isArray())
                .andExpect(MockMvcResultMatchers.jsonPath("$.errorList", IsCollectionWithSize.hasSize(0)));


        Mockito.verify(userService, VerificationModeFactory.times(1)).getUserById(ArgumentMatchers.any(Integer.class));


    }

}
