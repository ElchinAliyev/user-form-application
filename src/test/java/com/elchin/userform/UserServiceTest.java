package com.elchin.userform;


import com.elchin.userform.domain.User;
import com.elchin.userform.repository.UserRepository;
import com.elchin.userform.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.Optional;


@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {


    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    private User user = null;

    @Before
    public void setup() {

        user = new User();
        user.setName("Hezi");
        user.setSurname("Abbasov");
        user.setAge(21);

    }

    @Test
    public void insertUserTest() {
        Mockito.when(userRepository.save(ArgumentMatchers.any(User.class))).thenReturn(user);
        User user1 = userService.insertUser(user);

        Assert.assertEquals("User name must be same", "Hezi", user1.getName());
        Assert.assertEquals("User age must be same", 21, user1.getAge());
        Mockito.verify(userRepository, VerificationModeFactory.times(1)).save(ArgumentMatchers.any(User.class));

    }

    @Test
    public void getUserByIdTest() {
        Mockito.when(userRepository.findById(ArgumentMatchers.any(Integer.class))).thenReturn(Optional.of(user));

        Optional<User> optionalUser = userService.getUserById(ArgumentMatchers.any(Integer.class));

        Assert.assertNotNull("User can not be null", optionalUser.get());
        Assert.assertEquals("User name must be same", "Hezi", optionalUser.get().getName());
        Assert.assertEquals("User surname must be same", "Abbasov", optionalUser.get().getSurname());


        Mockito.verify(userRepository, VerificationModeFactory.times(1)).findById(ArgumentMatchers.any(Integer.class));


    }

    @Test
    public void addUserImageTest() {
        Mockito.when(userRepository.getOne(ArgumentMatchers.any(Integer.class))).thenReturn(user);
        Mockito.when(userRepository.save(ArgumentMatchers.any(User.class))).thenReturn(user);


        User user1 = userService.addUserImage(ArgumentMatchers.any(Integer.class),"My Address");

        Assert.assertNotNull("User can not be null",user);
        Assert.assertEquals("User name must be same","Hezi",user.getName());
        Assert.assertEquals("User surname must be same","Abbasov",user1.getSurname());


        Mockito.verify(userRepository, VerificationModeFactory.times(1)).getOne(ArgumentMatchers.any(Integer.class));
        Mockito.verify(userRepository, VerificationModeFactory.times(1)).save(ArgumentMatchers.any(User.class));
        Mockito.verifyNoMoreInteractions(userRepository);


    }

}

