package com.elchin.userform;

import com.elchin.userform.domain.User;
import com.elchin.userform.repository.UserRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@DataJpaTest
@RunWith(SpringRunner.class)
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    private User user = null;

    @Before
    public void setup() {
        user = new User();
        user.setName("Kamran");
        user.setSurname("Murtuzov");
        user.setAge(37);

    }


    @Test
    public void saveTest() {

        User user1 = userRepository.save(user);

        Assert.assertTrue("User id can not be empty", user1.getId() != 0);
        Assert.assertEquals("User name must be equal", "Kamran", user1.getName());
        Assert.assertEquals("User age must be equal", 37, user1.getAge());


    }


    @Test
    public void findByIdTest() {
        User user1 = userRepository.save(user);
        Assert.assertTrue("User id can not be empty", user1.getId() != 0);
        Optional<User> optionalUser = userRepository.findById(user1.getId());
        Assert.assertNotNull("User can not be null", optionalUser.get());
        Assert.assertEquals("User name must be same", "Kamran", user1.getName());
        Assert.assertEquals("User age must be equal", 37, user1.getAge());
    }
}
